package com.bilyoner.rmcoupon.couponservice.domain;

import com.bilyoner.rmcoupon.couponservice.domain.base.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USER")
@NoArgsConstructor
public class User extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Transient
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Coupon  coupon;

}
