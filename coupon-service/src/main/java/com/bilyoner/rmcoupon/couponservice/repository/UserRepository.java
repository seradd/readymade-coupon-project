package com.bilyoner.rmcoupon.couponservice.repository;

import com.bilyoner.rmcoupon.couponservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
