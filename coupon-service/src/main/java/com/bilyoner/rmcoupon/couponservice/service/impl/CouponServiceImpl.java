package com.bilyoner.rmcoupon.couponservice.service.impl;

import com.bilyoner.rmcoupon.converter.base.ResponseWrapperDTO;
import com.bilyoner.rmcoupon.couponservice.client.feign.BalanceServiceClient;
import com.bilyoner.rmcoupon.couponservice.config.amqp.RedisMessagePublisher;
import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.couponservice.domain.Event;
import com.bilyoner.rmcoupon.couponservice.domain.User;
import com.bilyoner.rmcoupon.couponservice.exception.CouponServiceException;
import com.bilyoner.rmcoupon.couponservice.helper.CouponCalculatorHelper;
import com.bilyoner.rmcoupon.couponservice.repository.CouponRepository;
import com.bilyoner.rmcoupon.couponservice.repository.EventRepository;
import com.bilyoner.rmcoupon.couponservice.repository.UserRepository;
import com.bilyoner.rmcoupon.couponservice.service.CouponService;
import com.bilyoner.rmcoupon.couponservice.validator.CouponValidator;
import com.bilyoner.rmcoupon.dto.couponservice.BuyCouponRequestDto;
import com.bilyoner.rmcoupon.dto.enumeration.CouponStatusEnum;
import com.bilyoner.rmcoupon.dto.enumeration.ResponseStatusEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@EnableCaching
@Service
public class CouponServiceImpl implements CouponService {

    private final CouponRepository couponRepository;
    private final EventRepository eventRepository;
    private final UserRepository userRepository;
    private final BalanceServiceClient balanceServiceClient;
    private final CouponCalculatorHelper couponCalculatorHelper;
    private final CouponValidator couponValidator;
    private final RedisMessagePublisher redisMessagePublisher;

    private static final String SUCCESS_MESSAGE = "İşlemi Başarılı.";

    @Cacheable(cacheNames = "events")
    @Override
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public ResponseWrapperDTO<Event> createCoupon(Coupon coupon) {
        ResponseWrapperDTO<Event> response = new ResponseWrapperDTO<>();
        response.setData(coupon.getCouponEvents());

        coupon.setCouponEvents(coupon.getCouponEvents().stream()
                .map(e -> eventRepository.findById(e.getId()).get()).collect(Collectors.toList()));

        String validationMessage = couponValidator.validateCoupon(coupon);
        if (!validationMessage.isEmpty()) {
            response.setMessage(validationMessage);
            response.setStatus(ResponseStatusEnum.FAIL);
            return response;
        }
        coupon.setStatus(CouponStatusEnum.CREATED);
        coupon.setCost(couponCalculatorHelper.calculateCouponCost(coupon));
        couponRepository.save(coupon);

        response.setStatus(ResponseStatusEnum.SUCCESS);
        response.setMessage(SUCCESS_MESSAGE);

        return response;
    }

    @CacheEvict(cacheNames = {"couponByUserId","couponByStatus"})
    @Override
    public ResponseWrapperDTO<Void> buyCoupon(BuyCouponRequestDto buyCouponRequest) {
        ResponseWrapperDTO<Void> response = new ResponseWrapperDTO<>();
        Long userId = buyCouponRequest.getUserId();
        couponValidator.checkUserExists(userId);
        String validationMessage = couponValidator.validateUserBalance(buyCouponRequest);
        if (!validationMessage.isEmpty()) {
            response.setStatus(ResponseStatusEnum.FAIL);
            response.setMessage(validationMessage);
            return response;
        }

        for(Long couponId: buyCouponRequest.getCoupons()) {
            Coupon coupon = findCoupon(couponId);
            String validationMessage1 = couponValidator.checkIsCouponSold(coupon);
            if (!validationMessage1.isEmpty()) {
                response.setStatus(ResponseStatusEnum.FAIL);
                response.setMessage(validationMessage1);
                return response;
            }
            User user = userRepository.findById(userId).orElse(new User());
            coupon.setUser(user);
            coupon.setStatus(CouponStatusEnum.SOLD);
            coupon.setPlayDate(LocalDateTime.now());
            couponRepository.save(coupon);
        }

        int couponTotalCost = buyCouponRequest.getCoupons().stream()
                .map(r -> Integer.valueOf(couponCalculatorHelper
                        .calculateCouponCost(findCoupon(userId)).toString()))
                .collect(Collectors.toList())
                .stream().reduce(0, Integer::sum);


        redisMessagePublisher.publish(userId +"-"+couponTotalCost);
        response.setStatus(ResponseStatusEnum.SUCCESS);
        response.setMessage(SUCCESS_MESSAGE);

        return response;
    }

    @Override
    public ResponseWrapperDTO<Void> cancelCoupon(Long couponId) {
        ResponseWrapperDTO<Void> response = new ResponseWrapperDTO<>();
        Coupon coupon = findCoupon(couponId);

        String validationMessage = couponValidator.validateCancelCoupon(coupon);

        if (!validationMessage.isEmpty()) {
            response.setMessage(validationMessage);
            response.setStatus(ResponseStatusEnum.FAIL);
            return response;
        }
        coupon.setStatus(CouponStatusEnum.CANCELLED);
        couponRepository.save(coupon);

        response.setMessage(SUCCESS_MESSAGE);
        response.setStatus(ResponseStatusEnum.SUCCESS);
        return response;
    }

    @Cacheable(cacheNames = "couponByStatus")
    @Override
    public List<Coupon> getCouponsByStatus(String status) {
        return couponRepository.findAllByStatus(CouponStatusEnum.valueOf(status)).orElse(Collections.emptyList());
    }

    @Cacheable(cacheNames = "couponByUserId")
    @Override
    public List<Coupon> getCouponsByUserId(Long userId) {
        try {
            return couponRepository.findAllByUserId(userId).orElse(Collections.emptyList());
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    private Coupon findCoupon(Long couponId) {
        return couponRepository.findById(couponId).orElseThrow(() -> new CouponServiceException
                .CouponNotFoundException(couponId.toString()));
    }

}
