package com.bilyoner.rmcoupon.couponservice.service;

import com.bilyoner.rmcoupon.converter.base.ResponseWrapperDTO;
import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.couponservice.domain.Event;
import com.bilyoner.rmcoupon.dto.couponservice.BuyCouponRequestDto;

import java.util.List;

public interface CouponService {

    List<Event> findAll();
    ResponseWrapperDTO<Void> buyCoupon(BuyCouponRequestDto buyCouponRequest);
    ResponseWrapperDTO<Void> cancelCoupon(Long couponId);
    List<Coupon> getCouponsByStatus(String status);
    List<Coupon> getCouponsByUserId(Long userId);
    ResponseWrapperDTO<Event> createCoupon(Coupon coupon);
}
