package com.bilyoner.rmcoupon.couponservice.repository;

import com.bilyoner.rmcoupon.couponservice.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
}
