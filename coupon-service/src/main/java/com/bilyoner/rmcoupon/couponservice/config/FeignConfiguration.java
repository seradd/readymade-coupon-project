package com.bilyoner.rmcoupon.couponservice.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.bilyoner.rmcoupon.couponservice")
public class FeignConfiguration {

    @Bean
    public feign.Logger.Level feignLoggingLevel() {
        return feign.Logger.Level.BASIC;
    }
}
