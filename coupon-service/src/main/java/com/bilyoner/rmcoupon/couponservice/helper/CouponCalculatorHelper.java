package com.bilyoner.rmcoupon.couponservice.helper;

import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@AllArgsConstructor
@ComponentScan
@Component
public class CouponCalculatorHelper {

    private static final Integer EVENT_PRICE = 5;

    public BigDecimal calculateCouponCost(Coupon coupon) {
        return BigDecimal.valueOf(coupon.getCouponEvents().size() * EVENT_PRICE);
    }
}
