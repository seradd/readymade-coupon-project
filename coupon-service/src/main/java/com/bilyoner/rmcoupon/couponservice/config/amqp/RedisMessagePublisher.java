package com.bilyoner.rmcoupon.couponservice.config.amqp;

public interface RedisMessagePublisher {

    void publish(final String message);
}
