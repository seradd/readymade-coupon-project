package com.bilyoner.rmcoupon.couponservice.client.feign;

import com.sun.istack.NotNull;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;

@FeignClient(name = "balance-service")
public interface BalanceServiceClient {

    @GetMapping(value = "/api/balance-service/get-user-balance/{userId}")
    ResponseEntity<BigDecimal> getUserBalance(@PathVariable @NotNull Long userId);

    @PostMapping(value = "/api/balance-service/update-user-balance/{userId}/{amount}")
    ResponseEntity<Void> updateUserBalance(@PathVariable @NotNull Long userId, @NotNull @PathVariable BigDecimal amount);
}
