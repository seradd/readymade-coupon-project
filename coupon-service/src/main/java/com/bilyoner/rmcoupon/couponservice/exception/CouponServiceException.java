package com.bilyoner.rmcoupon.couponservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CouponServiceException {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class CouponNotFoundException extends RuntimeException{
        public CouponNotFoundException(String message) {
            super("Coupon not found with the given couponId: " +message);
        }
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class UserNotFoundException extends RuntimeException{
        public UserNotFoundException(String message) {
            super("User not found with the given Id: " +message);
        }
    }
}