package com.bilyoner.rmcoupon.couponservice.domain;

import com.bilyoner.rmcoupon.couponservice.domain.base.BaseEntity;
import com.bilyoner.rmcoupon.dto.enumeration.EventTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "EVENT")
public class Event extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "MBS")
    private Integer mbs;

    @Enumerated(EnumType.STRING)
    @Column(name = "EVENT_TYPE")
    private EventTypeEnum eventType;

    @Column(name = "EVENT_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime eventDate;

    @ManyToMany(mappedBy = "couponEvents")
    private List<Coupon> coupons;

}
