package com.bilyoner.rmcoupon.couponservice.web.rest.converter;

import com.bilyoner.rmcoupon.converter.base.BaseConverter;
import com.bilyoner.rmcoupon.couponservice.domain.Event;
import com.bilyoner.rmcoupon.dto.couponservice.EventDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventConverter extends BaseConverter<Event, EventDto> {
}
