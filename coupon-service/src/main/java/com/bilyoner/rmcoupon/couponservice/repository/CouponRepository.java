package com.bilyoner.rmcoupon.couponservice.repository;

import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.dto.enumeration.CouponStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CouponRepository extends JpaRepository<Coupon, Long> {

    @Override
    Optional<Coupon> findById(Long id);

    Optional<List<Coupon>> findAllByStatus(CouponStatusEnum statusEnum);

    Optional<List<Coupon>> findAllByUserId(Long userId);
}
