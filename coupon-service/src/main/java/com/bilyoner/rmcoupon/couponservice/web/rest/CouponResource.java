package com.bilyoner.rmcoupon.couponservice.web.rest;

import com.bilyoner.rmcoupon.converter.base.ResponseWrapperDTO;
import com.bilyoner.rmcoupon.couponservice.repository.EventRepository;
import com.bilyoner.rmcoupon.couponservice.service.CouponService;
import com.bilyoner.rmcoupon.couponservice.web.rest.converter.CouponConverter;
import com.bilyoner.rmcoupon.couponservice.web.rest.converter.EventConverter;
import com.bilyoner.rmcoupon.dto.couponservice.BuyCouponRequestDto;
import com.bilyoner.rmcoupon.dto.couponservice.CouponDto;
import com.bilyoner.rmcoupon.dto.couponservice.EventDto;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/coupon-service")
@RequiredArgsConstructor
public class CouponResource {

    private final EventRepository eventRepository;
    private final EventConverter eventConverter;
    private final CouponConverter couponConverter;
    private final CouponService couponService;

    @PostMapping("create-coupon")
    public ResponseEntity<ResponseWrapperDTO> createCoupon(@Valid @RequestBody CouponDto couponDto){
        return ResponseEntity.ok().body(couponService.createCoupon(couponConverter.toEntity(couponDto)));
    }

    @GetMapping("get-events")
    public ResponseEntity<List<EventDto>> getEvents(){
        return ResponseEntity.ok().body(eventConverter.toDto(couponService.findAll()));
    }

    @PostMapping("buy-coupon")
    public ResponseEntity<ResponseWrapperDTO<Void>> buyCoupon(@Valid @RequestBody BuyCouponRequestDto buyCouponRequest){
        return ResponseEntity.ok().body(couponService.buyCoupon(buyCouponRequest));
    }

    @PatchMapping("cancel-coupon/{couponId}")
    public ResponseEntity<ResponseWrapperDTO<Void>> cancelCoupon(@PathVariable @NotNull Long couponId){
        return ResponseEntity.ok().body(couponService.cancelCoupon(couponId));
    }

    @GetMapping("get-coupons-by-status/{status}")
    public ResponseEntity<List<CouponDto>> getCouponsByStatus(@PathVariable @NotNull String status){
        return ResponseEntity.ok().body(couponConverter.toDto(couponService.getCouponsByStatus(status)));
    }

    @GetMapping("get-coupons-by-user/{userId}")
    public ResponseEntity<List<CouponDto>> getCouponsByUser(@PathVariable @NotNull Long userId){
        return ResponseEntity.ok().body(couponConverter.toDto(couponService.getCouponsByUserId(userId)));
    }
}
