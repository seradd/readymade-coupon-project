package com.bilyoner.rmcoupon.couponservice.domain;

import com.bilyoner.rmcoupon.couponservice.domain.base.BaseEntity;
import com.bilyoner.rmcoupon.dto.enumeration.CouponStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "COUPON")
public class Coupon extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", columnDefinition="varchar2 default 'CREATED'")
    private CouponStatusEnum status = CouponStatusEnum.CREATED;

    @Column(name = "COST")
    private BigDecimal cost;

    @Column(name = "PLAY_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime playDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.ALL
    })
    @JoinTable(name = "COUPON_SELECTION",
            joinColumns = @JoinColumn(name = "COUPON_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "EVENT_ID", referencedColumnName = "ID"))
    private List<Event> couponEvents;

}
