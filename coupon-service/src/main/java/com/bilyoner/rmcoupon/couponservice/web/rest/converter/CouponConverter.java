package com.bilyoner.rmcoupon.couponservice.web.rest.converter;

import com.bilyoner.rmcoupon.converter.base.BaseConverter;
import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.dto.couponservice.CouponDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CouponConverter extends BaseConverter<Coupon, CouponDto> {
}
