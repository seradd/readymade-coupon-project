package com.bilyoner.rmcoupon.couponservice.validator;

import com.bilyoner.rmcoupon.couponservice.client.feign.BalanceServiceClient;
import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.couponservice.domain.Event;
import com.bilyoner.rmcoupon.couponservice.exception.CouponServiceException;
import com.bilyoner.rmcoupon.couponservice.helper.CouponCalculatorHelper;
import com.bilyoner.rmcoupon.couponservice.repository.CouponRepository;
import com.bilyoner.rmcoupon.couponservice.repository.UserRepository;
import com.bilyoner.rmcoupon.dto.couponservice.BuyCouponRequestDto;
import com.bilyoner.rmcoupon.dto.enumeration.CouponStatusEnum;
import com.bilyoner.rmcoupon.dto.enumeration.EventTypeEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@ComponentScan
@Component
public class CouponValidator {

    private static final Integer MAX_CANCELLATION_TIME = 5;
    private final BalanceServiceClient balanceServiceClient;
    private final CouponRepository couponRepository;
    private final UserRepository userRepository;
    private final CouponCalculatorHelper couponCalculatorHelper;


    public String validateCoupon(Coupon coupon) {
        String message = "";
        List<Event> events = coupon.getCouponEvents();

        try {
            if (events.stream().anyMatch(event -> EventTypeEnum.FOOTBALL.equals(event.getEventType()))
                    && events.stream().anyMatch(event -> EventTypeEnum.TENNIS.equals(event.getEventType()))) {
                message = "Futbol ve Tenis karşılaşması aynı kupon içerisinde bulunamaz.";
            } else {
                int maxMbs = events.stream().map(Event::getMbs).collect(Collectors.toList()).stream()
                        .mapToInt(v -> v)
                        .max().orElse(0);
                if (maxMbs != 0 &&  events.size()<= maxMbs) {
                    message = "Kuponun oynanabilmesi için kupon içerisinde en az " + maxMbs + " adet karşılaşma olması zorunludur.";
                }
                if (events.stream().anyMatch(event -> LocalDateTime.now().isAfter(event.getEventDate()))) {
                    message = "Tarihi geçmiş karşılaşmalardan hazır kupon oluşturulamaz.";
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return message;
    }

    public String validateCancelCoupon(Coupon coupon) {
        String message = "";
        if(LocalDateTime.now().isBefore(coupon.getPlayDate().minusMinutes(MAX_CANCELLATION_TIME)))
            message = "Kupon oynama sonrası maksimum "+MAX_CANCELLATION_TIME+ " dk içerisinde kupon iptal edilebilir.";
        return message;
    }

    public String validateUserBalance(BuyCouponRequestDto buyCouponRequest) {
        String message = "";
        Long userId = buyCouponRequest.getUserId();
        BigDecimal amount = balanceServiceClient.getUserBalance(userId).getBody();

        int couponTotalCost = buyCouponRequest.getCoupons().stream()
                .map(r -> Integer.valueOf(couponCalculatorHelper.calculateCouponCost(findCoupon(r))
                        .toString())).collect(Collectors.toList())
                .stream().reduce(0, Integer::sum);

        if (amount != null && BigDecimal.valueOf(couponTotalCost).compareTo(amount) > 0)
            message = "Bakiye Yetersiz.";

        return message;
    }

    public String checkIsCouponSold(Coupon coupon) {
        String message = "";
        if(CouponStatusEnum.SOLD.equals(coupon.getStatus()))
            message = "Satılmış Kupon: id "+ coupon.getId();
        return message;
    }

    private Coupon findCoupon(Long couponId) {
        return couponRepository.findById(couponId).orElseThrow(() -> new CouponServiceException
                .CouponNotFoundException(couponId.toString()));
    }

    public void checkUserExists(Long userId) {
        userRepository.findById(userId).orElseThrow(() -> new CouponServiceException
                .UserNotFoundException(userId.toString()));
    }
}