drop table if exists coupon;
drop table if exists coupon_selection;
drop table if exists event;
drop table if exists user;
drop sequence if exists base_seq;
create sequence base_seq start with 1000 increment by 1;
create table coupon (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, cost decimal(19,2), play_date TIMESTAMP, status varchar2 default 'PAID', user_id NUMERIC, primary key (id));
create table coupon_selection (coupon_id NUMERIC not null, event_id NUMERIC not null, primary key (coupon_id, event_id));
create table event (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, event_date TIMESTAMP, event_type varchar(255), mbs decimal(19,2), name varchar(255), primary key (id));
create table user (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, name varchar(255), surname varchar(255), primary key (id));
alter table coupon add constraint FKmfuic7ht7p0xvyoxhq9oydhal foreign key (user_id) references user;
alter table coupon_selection add constraint FK6vxre71hsjhs9h9p5uqsj2cu1 foreign key (event_id) references event;
alter table coupon_selection add constraint FKlsay4bvgohnps5obf0o8rtqae foreign key (coupon_id) references coupon;

INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (1, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',1, 'Osasuna-Villereal');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (2, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',1, 'Fenerbahçe-Mallorca');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (3, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',1, 'Inter-Udinesse');

INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (4, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',2, 'Celtic-Rangers');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (5, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',2, 'Sounthander-Mancherster City');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (6, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',2, 'Chealsea-Sunderland');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (7, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',2, 'Manchester United-Arsenal');

INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (8, '2020-10-04 15:15:24', 1,'2020-10-03 15:15:24', '2020-10-08 15:15:24','FOOTBALL',3, 'Real Madrid-Barcelona');

//basketbol
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (9, '2020-10-04 15:15:24', 1,'2020-10-03 15:15:24', '2020-10-08 15:15:24','BASKETBALL',1, 'FenerBahçe-ÇSK');

INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (10, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','BASKETBALL',2, 'Kızılyıldız-Spartak Moscova');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (11, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','BASKETBALL',2, 'Efes-Zalgris');
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (12, '2020-10-04 15:15:24', 1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','BASKETBALL',2, 'Real Madrid-Panathinaikos');

INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (13, '2020-10-04 15:15:24', 1,'2020-10-03 15:15:24', '2020-10-08 15:15:24','BASKETBALL',1, 'Zenit-Baskonia');

//tenis
INSERT INTO EVENT (ID, CREATED_DATE, IS_ACTIVE, LAST_UPDATED, EVENT_DATE, EVENT_TYPE, MBS, NAME) VALUES (14, '2020-10-04 15:15:24',1, '2020-10-03 15:15:24', '2020-10-08 15:15:24','TENNIS',1, 'Djokovich-Nadal');

-- usert
INSERT INTO USER VALUES (1000, '2020-10-04 15:15:24',1, '2020-10-03 15:15:24', 'Serhat', 'Aydın');
COMMIT;