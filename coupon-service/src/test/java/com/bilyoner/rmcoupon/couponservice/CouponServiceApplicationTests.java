package com.bilyoner.rmcoupon.couponservice;

import com.bilyoner.rmcoupon.couponservice.domain.Coupon;
import com.bilyoner.rmcoupon.couponservice.domain.Event;
import com.bilyoner.rmcoupon.couponservice.repository.CouponRepository;
import com.bilyoner.rmcoupon.couponservice.repository.EventRepository;
import com.bilyoner.rmcoupon.couponservice.repository.UserRepository;
import com.bilyoner.rmcoupon.couponservice.service.CouponService;
import com.bilyoner.rmcoupon.couponservice.validator.CouponValidator;
import com.bilyoner.rmcoupon.dto.enumeration.EventTypeEnum;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
class CouponServiceApplicationTests {

	@Test
	void contextLoads() {
	}

	@MockBean
	CouponRepository couponRepository;
	@MockBean
	UserRepository userRepository;
	@MockBean
	EventRepository eventRepository;

	@MockBean
	CouponService couponService;


	@Autowired
	CouponValidator couponValidator;

	@Before
	public void setUp() {
		Coupon cancelCoupon = new Coupon();
		cancelCoupon.setPlayDate(LocalDateTime.now());

		Mockito.when(couponRepository.findById(1L).get())
				.thenReturn(cancelCoupon);
	}

	@Test
	void whenCancelCouponAfter5Min_thenNotEmpty() {
		Coupon cancelCoupon = new Coupon();
		cancelCoupon.setPlayDate(LocalDateTime.now().minusMinutes(-6));

		assertThat(couponValidator.validateCancelCoupon(cancelCoupon)).isNotEmpty();
	}

	@Test
	void whenTennisAndFootBallInSameCoupon_thenNotEmpty() {
		Coupon coupon = new Coupon();

		Event eventFootBall = new Event();
		eventFootBall.setEventType(EventTypeEnum.FOOTBALL);

		Event eventTennis = new Event();
		eventTennis.setEventType(EventTypeEnum.TENNIS);
		coupon.setCouponEvents(Arrays.asList(eventFootBall,eventTennis));

		assertThat(couponValidator.validateCoupon(coupon)).isNotEmpty();
	}

	@Test
	void whenExpiredGames_thenNotEmpty() {
		Coupon coupon = new Coupon();

		Event eventFootBall = new Event();
		eventFootBall.setEventType(EventTypeEnum.FOOTBALL);
		eventFootBall.setEventDate(LocalDateTime.now().minusMinutes(-10));

		Event eventTennis = new Event();
		eventTennis.setEventType(EventTypeEnum.TENNIS);
		eventTennis.setEventDate(LocalDateTime.now().minusMinutes(-10));
		coupon.setCouponEvents(Arrays.asList(eventFootBall,eventTennis));

		assertThat(couponValidator.validateCoupon(coupon)).isNotEmpty();
	}


}
