# Info
-Uygulamaları ayağa kaldırmadan önce;

- 1)root dizinde `docker-compose up` komutu ile `redisi` ayağa kaldırılır. 
- 2)config-server başlatılır.



## Postman Collection Url
- `url:`
https://www.getpostman.com/collections/b01c7b0b986b5b0edb33

## Database

* `******** Coupon Datase Info *********`
- `url:` http://localhost:8080/h2-console
- `-h2 console login parameters-`
- `Driver Class:`org.h2.Driver
- `JDBC URL:`jdbc:h2:mem:coupontestdb
- `User Name:`sa
- `password:` 1

* `******** Balance Datase Info *********`
- `url:` http://localhost:8081/h2-console
- `-h2 console login parameters-`
- `Driver Class:`org.h2.Driver
- `JDBC URL:`jdbc:h2:mem:balancetestdb
- `User Name:`sa
- `password:` 1


## Swagger
- `url:`
- http://localhost:8080/swagger-ui.html
- http://localhost:8081/swagger-ui.html


