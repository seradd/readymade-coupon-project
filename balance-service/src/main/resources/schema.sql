drop table if exists user;
drop table if exists user_balance;
drop table if exists user_balance_history;
drop sequence if exists base_seq;
create sequence base_seq start with 1000 increment by 1;
create table user (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, name varchar(255), surname varchar(255), primary key (id));
create table user_balance (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, amount decimal(19,2), user_id NUMERIC, primary key (id));
create table user_balance_history (id NUMERIC not null, created_date TIMESTAMP, is_active boolean not null, last_updated TIMESTAMP, amount decimal(19,2), type varchar(255), user_id NUMERIC, primary key (id));
alter table user_balance add constraint USER_ID_IDX unique (user_id);
alter table user_balance add constraint FK83kowcdadtmaxvkve517q74b8 foreign key (user_id) references user;
alter table user_balance_history add constraint FK6ikulb08ixgcfhlrerbhvim3p foreign key (user_id) references user;

-- usert
INSERT INTO USER VALUES (1000, '2020-10-04 15:15:24',1, '2020-10-03 15:15:24', 'Serhat', 'Aydın');
INSERT INTO USER_BALANCE (id, is_active, amount, user_id) VALUES (1000, 1, 60, 1000);
COMMIT;