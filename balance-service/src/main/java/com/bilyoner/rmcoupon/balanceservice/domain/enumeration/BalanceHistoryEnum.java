package com.bilyoner.rmcoupon.balanceservice.domain.enumeration;

public enum BalanceHistoryEnum {
    IN,
    OUT
}
