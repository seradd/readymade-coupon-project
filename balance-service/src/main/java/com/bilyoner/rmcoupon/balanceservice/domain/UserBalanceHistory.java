package com.bilyoner.rmcoupon.balanceservice.domain;

import com.bilyoner.rmcoupon.balanceservice.domain.base.BaseEntity;
import com.bilyoner.rmcoupon.balanceservice.domain.enumeration.BalanceHistoryEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Entity
@Table(name = "USER_BALANCE_HISTORY")
public class UserBalanceHistory extends BaseEntity {


    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private BalanceHistoryEnum type;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @ManyToOne
    private User user;

}
