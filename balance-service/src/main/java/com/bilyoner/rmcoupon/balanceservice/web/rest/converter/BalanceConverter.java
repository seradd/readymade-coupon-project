package com.bilyoner.rmcoupon.balanceservice.web.rest.converter;

import com.bilyoner.rmcoupon.balanceservice.domain.UserBalance;
import com.bilyoner.rmcoupon.converter.base.BaseConverter;
import com.bilyoner.rmcoupon.dto.balanceservice.UserBalanceDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BalanceConverter extends BaseConverter<UserBalance, UserBalanceDto> {
}
