package com.bilyoner.rmcoupon.balanceservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BalanceServiceException {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public static class UserBalanceNotFoundException extends RuntimeException{
        public UserBalanceNotFoundException(String message) {
            super(message);
        }
    }
}
