package com.bilyoner.rmcoupon.balanceservice.domain;

import com.bilyoner.rmcoupon.balanceservice.domain.base.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Entity
@Table(name = "USER_BALANCE", indexes = {
        @Index(name = "USER_ID_IDX", columnList = "USER_ID", unique = true)}
)
public class UserBalance extends BaseEntity {

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USER_ID")
    private User user;

}
