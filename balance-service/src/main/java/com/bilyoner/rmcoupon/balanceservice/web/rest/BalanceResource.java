package com.bilyoner.rmcoupon.balanceservice.web.rest;


import com.bilyoner.rmcoupon.balanceservice.service.BalanceService;
import com.bilyoner.rmcoupon.balanceservice.web.rest.converter.BalanceConverter;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/balance-service")
public class BalanceResource {

    private final BalanceConverter balanceConverter;
    private final BalanceService balanceService;

    @GetMapping(value = "/get-user-balance/{userId}")
    ResponseEntity<BigDecimal> getUserBalance(@PathVariable @NotNull Long userId){
        return ResponseEntity.ok().body(balanceService.getUserBalance(userId));
    }

    @PostMapping(value = "/update-user-balance/{userId}/{amount}")
    ResponseEntity<Void> updateUserBalance(@PathVariable @NotNull Long userId, @NotNull @PathVariable BigDecimal amount){
        balanceService.updateUserBalance(userId, amount);
        return ResponseEntity.ok().build();
    }
}
