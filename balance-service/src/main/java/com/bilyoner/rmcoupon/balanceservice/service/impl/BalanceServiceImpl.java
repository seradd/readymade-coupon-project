package com.bilyoner.rmcoupon.balanceservice.service.impl;

import com.bilyoner.rmcoupon.balanceservice.domain.UserBalance;
import com.bilyoner.rmcoupon.balanceservice.domain.UserBalanceHistory;
import com.bilyoner.rmcoupon.balanceservice.domain.enumeration.BalanceHistoryEnum;
import com.bilyoner.rmcoupon.balanceservice.repository.BalanceRepository;
import com.bilyoner.rmcoupon.balanceservice.repository.UserBalanceHistoryRepository;
import com.bilyoner.rmcoupon.balanceservice.service.BalanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Configurable
@Service
public class BalanceServiceImpl implements BalanceService {

    private final BalanceRepository balanceRepository;
    private final UserBalanceHistoryRepository  userBalanceHistoryRepository;

    @Override
    public BigDecimal getUserBalance(Long userId) {
        UserBalance userBalance = findUserBalance(userId);
        return userBalance.getAmount();
    }

    @Override
    public void updateUserBalance(Long userId, BigDecimal amount) {
        UserBalance userBalance = balanceRepository.findDistinctTopByUserId(userId).orElse(new UserBalance());
        userBalance.setAmount(userBalance.getAmount().subtract(amount));
        UserBalanceHistory userBalanceHistory = new UserBalanceHistory();

        userBalanceHistory.setAmount(amount);
        userBalanceHistory.setUser(userBalance.getUser());
        userBalanceHistory.setType(BalanceHistoryEnum.OUT);

        balanceRepository.save(userBalance);
        userBalanceHistoryRepository.save(userBalanceHistory);
    }

    protected UserBalance findUserBalance(Long userId) {
        return balanceRepository.findDistinctTopByUserId(userId).orElse(null);
    }
}
