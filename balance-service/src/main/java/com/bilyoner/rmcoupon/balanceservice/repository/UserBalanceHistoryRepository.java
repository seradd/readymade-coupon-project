package com.bilyoner.rmcoupon.balanceservice.repository;

import com.bilyoner.rmcoupon.balanceservice.domain.UserBalanceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBalanceHistoryRepository extends JpaRepository<UserBalanceHistory, Long> {

}
