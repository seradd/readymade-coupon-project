package com.bilyoner.rmcoupon.balanceservice.config.amqp;

import com.bilyoner.rmcoupon.balanceservice.service.BalanceService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@ComponentScan(basePackages = "com.bilyoner.rmcoupon.service")
@NoArgsConstructor
@Service
public class RedisMessageSubscriber implements MessageListener {

    @Autowired
    private BalanceService balanceService;

    @Autowired
    public RedisMessageSubscriber(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    public static List<String> messageList = new ArrayList<>();

    @Override
    public void onMessage(final Message message, final byte[] pattern) {
        messageList.add(message.toString());
        log.info("Message received: " + new String(message.getBody()));
        String[] m = messageList.get(0).split("-");
        balanceService.updateUserBalance(Long.valueOf(m[0]), BigDecimal.valueOf(Integer.parseInt(m[1])));
    }


}