package com.bilyoner.rmcoupon.balanceservice.domain;

import com.bilyoner.rmcoupon.balanceservice.domain.base.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "USER")
public class User extends BaseEntity {


    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @OneToOne(mappedBy = "user")
    private UserBalance userBalance;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USER_ID")
    private Set<UserBalanceHistory> userBalanceHistory;

}
