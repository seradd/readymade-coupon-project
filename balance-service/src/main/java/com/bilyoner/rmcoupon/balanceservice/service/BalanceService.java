package com.bilyoner.rmcoupon.balanceservice.service;


import java.math.BigDecimal;

public interface BalanceService {


    BigDecimal getUserBalance(Long userId);

    void updateUserBalance(Long userId, BigDecimal amount);
}
