package com.bilyoner.rmcoupon.balanceservice.repository;

import com.bilyoner.rmcoupon.balanceservice.domain.UserBalance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BalanceRepository extends JpaRepository<UserBalance, Long> {

    Optional<UserBalance> findDistinctTopByUserId(Long userId);
}
