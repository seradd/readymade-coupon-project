package com.bilyoner.rmcoupon.balanceservice;

import com.bilyoner.rmcoupon.balanceservice.domain.User;
import com.bilyoner.rmcoupon.balanceservice.domain.UserBalance;
import com.bilyoner.rmcoupon.balanceservice.repository.BalanceRepository;
import com.bilyoner.rmcoupon.balanceservice.repository.UserBalanceHistoryRepository;
import com.bilyoner.rmcoupon.balanceservice.repository.UserRepository;
import com.bilyoner.rmcoupon.balanceservice.service.BalanceService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

@ActiveProfiles("dev")
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
class BalanceServiceApplicationTests {

    @Test
    void contextLoads() {
    }

    @MockBean
    private BalanceRepository balanceRepository;
	@MockBean
	private UserBalanceHistoryRepository userBalanceHistoryRepository;
	@MockBean
	private UserRepository userRepository;


	@Before
    public void setUp() {
        User serhat = new User();
		serhat.setId(1L);
		serhat.setName("Serhat");
        Mockito.when(userRepository.findById(1L).get()).thenReturn(serhat);


		UserBalance userBalance = new UserBalance();
		userBalance.setUser(serhat);
		userBalance.setAmount(BigDecimal.valueOf(60));
    }

	@Test
	public void when() {

	}



}
