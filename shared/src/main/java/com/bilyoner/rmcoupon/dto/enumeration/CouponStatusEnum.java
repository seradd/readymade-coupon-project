package com.bilyoner.rmcoupon.dto.enumeration;

public enum CouponStatusEnum {
    CREATED,
    CANCELLED,
    SOLD
}
