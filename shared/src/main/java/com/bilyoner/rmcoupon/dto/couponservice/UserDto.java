package com.bilyoner.rmcoupon.dto.couponservice;

import com.bilyoner.rmcoupon.dto.base.BaseDto;
import lombok.Data;

@Data
public class UserDto extends BaseDto {

    private String name;
    private String surname;
    private CouponDto coupon;
}
