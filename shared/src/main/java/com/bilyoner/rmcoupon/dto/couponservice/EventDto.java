package com.bilyoner.rmcoupon.dto.couponservice;

import com.bilyoner.rmcoupon.dto.base.BaseDto;
import com.bilyoner.rmcoupon.dto.enumeration.EventTypeEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EventDto extends BaseDto {

    private String name;
    private Integer mbs;
    private EventTypeEnum eventType;
    private LocalDateTime eventDate;
}
