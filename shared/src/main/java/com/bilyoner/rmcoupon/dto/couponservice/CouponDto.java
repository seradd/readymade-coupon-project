package com.bilyoner.rmcoupon.dto.couponservice;

import com.bilyoner.rmcoupon.dto.base.BaseDto;
import com.bilyoner.rmcoupon.dto.enumeration.CouponStatusEnum;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class CouponDto extends BaseDto {

    private CouponStatusEnum status;
    private BigDecimal cost;
    private LocalDateTime playDate;
    private UserDto user;
    private List<EventDto> couponEvents;
}
