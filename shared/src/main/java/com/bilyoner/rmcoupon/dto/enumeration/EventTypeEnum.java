package com.bilyoner.rmcoupon.dto.enumeration;

public enum EventTypeEnum {
    FOOTBALL,
    BASKETBALL,
    TENNIS
}
