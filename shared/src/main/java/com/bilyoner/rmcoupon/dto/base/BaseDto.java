package com.bilyoner.rmcoupon.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

@Data
public class BaseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id ;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDateTime updateDate;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDateTime createDate;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<String, Map<String, String>> translations;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BaseDto baseDto = (BaseDto) o;
        if (baseDto.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), baseDto.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

}
