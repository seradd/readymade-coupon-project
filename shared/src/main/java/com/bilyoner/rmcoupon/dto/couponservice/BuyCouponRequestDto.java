package com.bilyoner.rmcoupon.dto.couponservice;

import lombok.Data;

import java.util.List;

@Data
public class BuyCouponRequestDto {
    private Long userId;
    private List<Long> coupons;
}
