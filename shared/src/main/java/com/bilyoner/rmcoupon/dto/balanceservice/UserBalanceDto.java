package com.bilyoner.rmcoupon.dto.balanceservice;

import com.fasterxml.jackson.databind.ser.Serializers;
import lombok.Data;

@Data
public class UserBalanceDto extends Serializers.Base {
    private String sessionId;

}
