package com.bilyoner.rmcoupon.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static LocalDateTime toDateTime(String stringDate, String pattern) {
        return LocalDateTime.parse(stringDate, DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDate toDate(String stringDate, String pattern) {
        return LocalDate.parse(stringDate, DateTimeFormatter.ofPattern(pattern));
    }

    public static String toString(LocalDate date, String pattern) {
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }
}
