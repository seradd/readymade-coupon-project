package com.bilyoner.rmcoupon.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@Slf4j
public class JsonUtil {

    public static <T> T fileToObjectConverter(String stringToConvert, Class<? extends T> clazz, boolean isFromJSON) {
        try {
            ObjectMapper mapper;
            if(isFromJSON){
                mapper = new ObjectMapper();
            } else {
                mapper = new XmlMapper();
            }

            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            T objectTobeConverted = clazz.getConstructor().newInstance();
            objectTobeConverted = mapper.readValue(stringToConvert, clazz);

            return objectTobeConverted;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }catch (InstantiationException e) {
            log.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            log.error(e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            log.error(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
