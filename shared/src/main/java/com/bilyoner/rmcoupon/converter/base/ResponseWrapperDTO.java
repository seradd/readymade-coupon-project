package com.bilyoner.rmcoupon.converter.base;

import com.bilyoner.rmcoupon.dto.enumeration.ResponseStatusEnum;
import lombok.Data;

import java.util.List;

@Data
public class ResponseWrapperDTO<T> {
    private ResponseStatusEnum status;
    private String message;
    private List<T> data;

}
